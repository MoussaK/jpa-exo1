package org.konate.tpjpa.serie1.exo1;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.konate.tpjpa.serie1.exo1.model.User;
import org.konate.tpjpa.serie1.exo1.model.UserFactory;

public class Main {

	public static void main(String[] args) {

		EntityManagerFactory entityManagerFactory = 
				Persistence.createEntityManagerFactory("jpa-test");
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		Calendar calendar = new GregorianCalendar(1980, 5, 20);
		Date date = calendar.getTime();
		
		UserFactory uf = new UserFactory(entityManager);
		User user = uf.createUser("Kylian", "MBAPPE", date);
		
		System.out.println(user);
		System.out.println(uf.find(4));
		uf.delete(4);
	}
}








