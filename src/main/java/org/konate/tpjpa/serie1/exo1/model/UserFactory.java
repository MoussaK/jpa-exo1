package org.konate.tpjpa.serie1.exo1.model;

import java.util.Date;

import javax.persistence.EntityManager;

public class UserFactory {
	private EntityManager entityManager;

	public UserFactory(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	
	public User createUser(String firstName, String lastName, Date dateOfBirth) {
		User user = new User(firstName, lastName, dateOfBirth);
		entityManager.getTransaction().begin();
		entityManager.persist(user);
		entityManager.getTransaction().commit();
		
		return user;
	}
	
	public User find(long id) {
		//Not necessary but performance gain whlie reading during transaction
		//entityManager.getTransaction().begin();
		User user = entityManager.find(User.class, id);
		//entityManager.getTransaction().commit();
		
		return user;
		
	}
	
	public void delete(long id) {
		entityManager.getTransaction().begin();
		entityManager.remove(find(id));
		entityManager.getTransaction().commit();
	}
}
